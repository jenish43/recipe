<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        $app_id = $request->header('app-id');
        $app_secret = $request->header('app-secret'); 
        $auth_token = $request->header('auth-token');               
        
        if(is_null($app_id) || is_null($app_secret)) {
            return response()->json([
                'status' => 'error',
                'declaration' => 'app_credentials_missing',
                'payload' => [ "message" => "App credentials are missing." ],
            ], 401);
        }
        
        if(config('recipe.app_id') != $app_id || config('recipe.app_secret') != $app_secret){
            return response()->json([
                'status' => 'error',
                'declaration' => 'invalid_app_credentials',
                'payload' => [ "message" => "App credentials are invalid." ],
            ], 401);
        }
        
        if(is_null($auth_token) || empty($auth_token)) {
            return response()->json([
                'status' => 'error',
                'declaration' => 'auth_token_missing',
                //'payload' => [],
            ], 401);
        }
       
        try {
            $decrypted = \Illuminate\Support\Facades\Crypt::decrypt($auth_token);
            $user_email = $decrypted['email'];            
        } catch (DecryptException $e) {
            return response()->json([
                'status' => 'error',
                'declaration' => 'auth_token_invalid',
                //'payload' => [],
            ], 401);
        }
        
        
        $user = \App\Models\Users::where([
                'auth_token' => $auth_token,
                'email' => $user_email,                
            ])->first();
            
        if(is_null($user)) {
            return response()->json([
                'status' => 'error',
                'declaration' => 'auth_token_invalid',
                'payload' => [ "message" => "Your session has timed out. Please log in again."],
            ], 401);
        }

        $request->merge(compact('user'));
        return $next($request);
    }
}
