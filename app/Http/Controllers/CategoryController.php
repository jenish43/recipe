<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Create Category
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function create(Request $request) {

        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        $name = $request->input('name');
        $category = Category::firstOrCreate([
            "name" => $name
        ]);

        if (!$category->wasRecentlyCreated) {
            return response()->json([
                        'status' => 'error',
                        'declaration' => 'category_already_exists',
                        'payload' => ['message' => "These name already exists you can rename it."],
                            ], 404);
        }

        return response()->json([
                    'status' => 'success',
                    'declaration' => 'category_created',
                    'payload' => [
                        "message" => "Category created successfully..!",
                        'category' => $category
                    ],
                        ], 200);
    }

    /**
     * List Category
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function listAll(Request $request) {
        
        
        $category = Category::all();
        
        return response()->json([
            'status' => 'success',
            'declaration' => 'category_created',
            'payload' => [
                "message" => "Category created successfully..!",
                'category' => $category
            ],
        ], 200);
    }
    
    /**
     * List Category
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function delete(Request $request,$category_id) {
        
        
        $category = Category::find($category_id);
        if(!$category){
            return response()->json([
                'status' => 'error',
                'declaration' => 'category_not_found',
                'payload' => ['message' => "Category not found..!"],
            ], 404);
        }        
        $category->delete();        
        return response()->json([
            'status' => 'success',
            'declaration' => 'category_created',
            'payload' => [
                "message" => "Category deleted successfully..!",               
            ],
        ], 200);
    }
}
