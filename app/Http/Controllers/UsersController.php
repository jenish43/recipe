<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Intervention\Image\ImageManagerStatic as Image;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Login
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function login(Request $request) {
                
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = Users::where('email', '=', $email)->first();

        if (is_null($user) ||
            !Hash::check($password, $user->password)
        ) {
            return response()->json([
                'status' => 'error',
                'declaration' => 'invalid_user',
                'payload' => ['message' => "Invalid Username or password. Please try again."],
            ], 404);
        }

        $encrypt = [
            'user_id' => $user->id,            
            'email' => $user->email,
            'created_at' => Carbon::now(),
        ];

        $token = Crypt::encrypt($encrypt);
        $user->update([
            'auth_token' => $token
        ]);

        return response()->json([
            'status' => 'success',
            'declaration' => 'auth_token_generated',
            'payload' => [
                "message" => "You are successfully logged in.",
                'user' => [
                    'id' => $user->id,
                    'thumb_image' => $user->thumb,
                    'full_image' => $user->full_image,
                    'name' => $user->name,
                    'email' => $user->email,                        
                ],
                'auth_token' => $token
            ],
        ], 200);
    }
    
    /**
     * Create User
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function create(Request $request) {
        
        $this->validate($request, [            
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',            
            'password' => 'required|string|min:8',
            'image' => 'nullable|file'            
        ]);
        
        $name = $request->input('name');        
        $email = $request->input('email');
        $password = $request->input('password');
        
        $user = Users::where('email', $email)->first();
        if(!is_null($user)){
            return response()->json([
                'status' => 'error',
                'declaration' => 'user_already_exist',
                'payload' => ['message' => "User already exist. Please try with defferent email."],
            ], 404);
        }
        
        $thumb_url = NULL;
        $full_url = NULL;
        
        if ($request->hasFile('image')) {            
            $image = $request->file('image');            
            $filename = $image->getClientOriginalName();            
            $destinationPath = public_path('assets/images/user/');                        
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(150, 150);
            $image_resize->save($destinationPath . '/thumb_' . $filename);            
            $image->move($destinationPath, $filename);
            $thumb_url = url() . '/assets/images/user/thumb_' . $filename;
            $full_url = url() .  '/assets/images/user/'. $filename;            
        }

        $password = Hash::make($password);
        $create_user = Users::firstOrCreate([                
                "email" => $email                
            ],[
                "password" => $password,
                "name" => $name,
                'thumb_image' => $thumb_url,
                'full_image' => $full_url,
                
        ]);
        
        if (!$create_user->wasRecentlyCreated ) {
            return response()->json([
                'status' => 'error',
                'declaration' => 'user_already_exists',
                'payload' => ['message' => "These email already exists you can login."],
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'declaration' => 'user_created',
            'payload' => [
                "message" => "User created successfully you can now logged in.",
                'user' => $create_user                
            ],
        ], 200);
    }
}
