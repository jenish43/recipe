<?php

namespace App\Http\Controllers;


use App\Models\Recipes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Create Recipe
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function create(Request $request) {
        
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'type' => 'nullable|string|max:255',
            'category_id' => 'required|exists:category,id',
            'defficulty' => 'required|in:easy,medium,hard',            
            'description' => 'nullable|string|max:255',
            'photo' => 'nullable|file',
            'preparation_time' => 'required|string|max:255',
            'cooking_time' => 'required|string|max:255',            
            'serves' => 'required|int',
            'nutritions' => 'required|array',
            'nutritions.*.name' => 'required|string',
            'nutritions.*.quantity' => 'required|int',
            'ingradients' => 'required|array',
            'ingradients.*.name' => 'required|string',
            'ingradients.*.quantity' => 'required|int',
            'steps' => 'required|array',
            'steps.*.text' => 'required|string',
        ]);

        $title = $request->input('title');
        $category_id = $request->input('category_id');
        $type = $request->input('type');                
        $user = $request->input('user');        
        $description = $request->input('description');
        $photo = $request->file('photo');
        $preparation_time = $request->input('preparation_time');
        $cooking_time = $request->input('cooking_time');
        $defficulty = $request->input('defficulty');
        $serves = $request->input('serves');
        $nutritions = $request->input('nutritions');
        $ingradients = $request->input('ingradients');
        $steps = $request->input('steps');
        
        $thumb_url = NULL;
        $full_url = NULL;
        
        if ($request->hasFile('photo')) {            
            $image = $request->file('photo');            
            $filename = $image->getClientOriginalName();            
            $destinationPath = public_path('assets/images/recipe/');                        
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(150, 150);
            $image_resize->save($destinationPath . '/thumb_' . $filename);            
            $image->move($destinationPath, $filename);
            $thumb_url = url() . '/assets/images/recipe/thumb_' . $filename;
            $full_url = url() .  '/assets/images/recipe/'. $filename;            
        }
        
        
        $category = \App\Models\Category::find($category_id);
        if (is_null($category)) {
            return response()->json([
                        "status" => "error",
                        "declaration" => "category_not_found",
                        "payload" => [],
                            ], 404);
        }
                
        $recipe = $category->recipes()->firstOrCreate([
            "title" => $title,
            "user_id" => $user->id,
            "type" => $type,
            "published" => 1,
                ], [
            "description" => $description,
            "photo" => $full_url,
            "photo_thumb" => $thumb_url,
            "preparation_time" => $preparation_time,
            "cooking_time" => $cooking_time,
            "defficulty" => $defficulty,
            "serves" => $serves                        
        ]);

        if (!$recipe->wasRecentlyCreated) {
            return response()->json([
                        'status' => 'error',
                        'declaration' => 'recipe_already_exists',
                        'payload' => ['message' => "These recipe already exists."],
                            ], 404);
        }
        
        $recipe->ingradients()->createMany($ingradients);
        $recipe->nutritions()->createMany($nutritions);
        $recipe->steps()->createMany($steps);
        
        $final = Recipes::with(['category','ingradients','nutritions','steps'])->where('id',$recipe->id)->get();
        return response()->json([
                    'status' => 'success',
                    'declaration' => 'recipe_created',
                    'payload' => [
                        "message" => "Recipe created successfully..!",
                        'recipe' => $final
                    ],
                        ], 200);
    }

    /**
     * List Category
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function listAll(Request $request) {
        
        $recipes = Recipes::with(['category','ingradients','nutritions','steps'])->where('published',true)->get();
        
        if($recipes->isEmpty()){
            return response()->json([
                'status' => 'success',
                'declaration' => 'recipes_not_found',
                'payload' => [
                    "message" => "Recipe not found..!",
                ],
            ], 200);
        }
        
        return response()->json([
            'status' => 'success',
            'declaration' => 'recipes_found',
            'payload' => [                
                'recipes' => $recipes
            ],
        ], 200);
    }
    
    /**
     * List Recipes
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function delete(Request $request,$recipe_id) {
        
        
        $recipes = Recipes::find($recipe_id);
        $user = $request->input('user');
        
        if(!$recipes){
            return response()->json([
                'status' => 'error',
                'declaration' => 'recipe_not_found',
                'payload' => ['message' => "Recipe not found..!"],
            ], 404);
        }
        
        if($recipes->user_id != $user->id){
            return response()->json([
                'status' => 'error',
                'declaration' => 'user_not_found',
                'payload' => ['message' => "You can delete this recipe..!"],
            ], 404);
        }
        
        $recipes->delete();        
        return response()->json([
            'status' => 'success',
            'declaration' => 'recipe_created',
            'payload' => [
                "message" => "Recipe deleted successfully..!",               
            ],
        ], 200);
    }
    
    /**
     * Published Recipe
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function publish(Request $request,$recipe_id) {
        
        $recipes = Recipes::find($recipe_id);
        $user = $request->input('user');
        
        if(!$recipes){
            return response()->json([
                'status' => 'error',
                'declaration' => 'recipe_not_found',
                'payload' => ['message' => "Recipe not found..!"],
            ], 404);
        }
        
        if($recipes->user_id != $user->id){
            return response()->json([
                'status' => 'error',
                'declaration' => 'user_not_found',
                'payload' => ['message' => "You can delete this recipe..!"],
            ], 404);
        }
        
        if($recipes->published){
            return response()->json([
                'status' => 'error',                
                'declaration' => 'recipe_already_published',
                'payload' => ['message' => "Recipe is already published..!"],
            ], 404);
        }
        
        $recipes->published = true;
        $recipes->save();
        
        return response()->json([
            'status' => 'success',
            'declaration' => 'recipe_published',
            'payload' => [
                "message" => "Recipe published successfully..!",               
            ],
        ], 200);
    }
    
    /**
     * Unpublished Recipe
     * 
     * @param   Request $request
     * @return  \Illuminate\Http\Response
     * @author  Jenish Panchal <jenishpanchal43@gmail.com>
     */
    public function unpublish(Request $request,$recipe_id) {
        
        $recipes = Recipes::find($recipe_id);
        $user = $request->input('user');
        
        if(!$recipes){
            return response()->json([
                'status' => 'error',
                'declaration' => 'recipe_not_found',
                'payload' => ['message' => "Recipe not found..!"],
            ], 404);
        }
        
        if($recipes->user_id != $user->id){
            return response()->json([
                'status' => 'error',
                'declaration' => 'user_not_found',
                'payload' => ['message' => "You can delete this recipe..!"],
            ], 404);
        }
        
        if(!$recipes->published){
            return response()->json([
                'status' => 'error',                
                'declaration' => 'recipe_already_unpublished',
                'payload' => ['message' => "Recipe is already unpublished..!"],
            ], 404);
        }
        
        $recipes->published = false;
        $recipes->save();
        
        return response()->json([
            'status' => 'success',
            'declaration' => 'recipe_published',
            'payload' => [
                "message" => "Recipe unpublished successfully..!",               
            ],
        ], 200);
    }
}
