<?php

/**
 * Recipes.php
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * RecipeSteps class
 *
 * @author Jenish Panchal <jenishp@unoindia.co>
 * @codeCoverageIgnore
 */
class RecipeSteps extends Model {

    use SoftDeletes;

    /**
     * The database table name.
     *
     * @var     string
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $table = 'recipe_steps';

    /**
     * The attributes that are mass assignable.
     *
     * @var     array
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $fillable = [
        'recipe_id', 'text'
    ];
    
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    
    /**
     * Associated Recipes.
     *
     * @return  \Illuminate\Database\Relation\HasMany
     */
    public function recipes() {
        return $this->belongsTo('App\Models\Recipes','id','recipe_id');
    }
}
