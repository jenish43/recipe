<?php

/**
 * Recipes.php
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Recipes class
 *
 * @author Jenish Panchal <jenishp@unoindia.co>
 * @codeCoverageIgnore
 */
class Recipes extends Model {

    use SoftDeletes;

    /**
     * The database table name.
     *
     * @var     string
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $table = 'recipes';

    /**
     * The attributes that are mass assignable.
     *
     * @var     array
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $fillable = [
        'category_id', 'user_id', 'title', 'type', 'description', 'photo', 'photo_thumb', 'preparation_time', 'cooking_time', 'defficulty', 'serves', 'published'
    ];
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Associated User.
     *
     * @return  \Illuminate\Database\Relation\belongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\Users');
    }
    
    
    /**
     * Associated ingradients.
     *
     * @return  \Illuminate\Database\Relation\hasMany
     */
    public function ingradients() {
        return $this->hasMany('App\Models\Nutritions','recipe_id');
    }
    
    /**
     * Associated nutritions.
     *
     * @return  \Illuminate\Database\Relation\hasMany
     */
    public function nutritions() {
        return $this->hasMany('App\Models\Ingredients','recipe_id');
    }
    
    /**
     * Associated category.
     *
     * @return  \Illuminate\Database\Relation\belongsTo
     */
    public function category() {
        return $this->belongsTo('App\Models\Category');
    }
    
    /**
     * Associated category.
     *
     * @return  \Illuminate\Database\Relation\hasMany
     */
    public function steps() {
        return $this->hasMany('App\Models\RecipeSteps','recipe_id');
    }
}
