<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model {
    
    use SoftDeletes;
    
    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'email', 'password', 'auth_token', 'thumb_image','full_image'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    protected $hidden = ['deleted_at'];
    
    
    /**
     * Associated Recipes.
     *
     * @return  \Illuminate\Database\Relation\HasMany
     */
    public function recipes() {
        return $this->hasMany('App\Models\Recipes','user_id');
    }
}
