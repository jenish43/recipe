<?php

/**
 * Category.php
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Category class
 *
 * @author Jenish Panchal <jenishp@unoindia.co>
 * @codeCoverageIgnore
 */
class Category extends Model {

    use SoftDeletes;

    /**
     * The database table name.
     *
     * @var     string
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var     array
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $fillable = [
        'name',        
    ];
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    
    /**
     * Associated Recipes.
     *
     * @return  \Illuminate\Database\Relation\HasMany
     */
    public function recipes() {
        return $this->hasMany('App\Models\Recipes');
    }
    
}
