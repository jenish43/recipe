<?php

/**
 * Ingredients.php
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Ingredients class
 *
 * @author Jenish Panchal <jenishp@unoindia.co>
 * @codeCoverageIgnore
 */
class Nutritions extends Model {

    use SoftDeletes;

    /**
     * The database table name.
     *
     * @var     string
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $table = 'nutritions';

    /**
     * The attributes that are mass assignable.
     *
     * @var     array
     * @author  Jenish Panchal <jenishp@unoindia.co>
     */
    protected $fillable = [
        'recipe_id', 'name', 'quantity'
    ];
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    
    /**
     * Associated Recipes.
     *
     * @return  \Illuminate\Database\Relation\HasMany
     */
    public function recipes() {
        return $this->belongsTo('App\Models\Recipes','id','recipe_id');
    }
    
}
