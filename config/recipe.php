<?php

return [

    /*
    |--------------------------------------------------------------------------
    | dfx .env defaults
    |--------------------------------------------------------------------------
    |
    | Here we set up default values for .env so localhost doesn't break
    | just because someone didn't update their own .env file.
    |
    |
    | IMPORTANT :
    |
    | Your .env values will override these default config values, so please
    | make changes there instead so you don't accidentally commit your
    | localhost setup to the repository.
    |
    */

    'app_id' => env('APP_ID', '3mPT6HaDkFPYTzS'),
    'app_secret' => env('APP_SECRET', '0FBHRn4sZ8Qw0M2a5f5pKqDFlmX0DCj3E3y'),    
];