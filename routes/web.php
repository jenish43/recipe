<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key/{digit:[1-9][0-9]*}', function($digit) {    
    return str_random($digit);
});

//,'middleware' => 'auth'

$router->group(['prefix' => 'user'], function($router) {
    $router->post('/login', 'UsersController@login');
    $router->post('/signup', 'UsersController@create');
});


$router->group(['prefix' => 'category','middleware' => 'auth'], function($router) {    
    $router->put('/add', 'CategoryController@create');
    $router->delete('/delete/{id:[1-9][0-9]*}', 'CategoryController@delete');
});

$router->group(['prefix' => 'recipe','middleware' => 'auth'], function($router) {
    $router->post('/add', 'RecipeController@create');
    $router->delete('/delete/{id:[1-9][0-9]*}', 'RecipeController@delete');    
    $router->post('/publish/{id:[1-9][0-9]*}', 'RecipeController@publish');
    $router->post('/unpublish/{id:[1-9][0-9]*}', 'RecipeController@unpublish');
});

$router->group(['prefix' => 'category'], function($router) {
    $router->get('/list', 'CategoryController@listAll');
});

$router->group(['prefix' => 'recipe'], function($router) {
    $router->get('/list', 'RecipeController@listAll');
});